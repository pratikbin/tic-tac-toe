# Tic Tac Toe

## Description

This is a simple tic tac toe game made with nodejs and socket.io

## How to run

### Prerequisites

```shell
npm i
npm run compile
```

### Server

```shell
# npm run start <PORT>
npm run start 3008
```

### Client/Players

```shell
## npm run client <HOST> <PORT>

## Plyer 1, if running server in same system
npm run client 127.0.0.0 3008

## Plyer 2, if running server in same system
npm run client 127.0.0.0 3008
```

## Development

### lint

```shell
npm run lint
```
