const winPatterns: number[][] = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

// Check if the game is won
export function checkWin(gameBoard: (string | null)[]): boolean {
  for (const pattern of winPatterns) {
    if (gameBoard[pattern[0]]
      && gameBoard[pattern[0]] === gameBoard[pattern[1]]
      && gameBoard[pattern[0]] === gameBoard[pattern[2]]) {
        return true;
      }
  }
  return false;
}

// Check if the game is a draw
export function checkDraw(gameBoard: (string | null)[]): boolean {
  return gameBoard.every((cell) => cell !== null);
}
