// conversion
// We use import statements to import the required modules (readline and socket.io-client) instead of require.
// We add type annotations to function arguments and variables where necessary to specify the data types.
// We fix the issue with the parseInt call in the makeMove function by using parseInt(answer) instead of parseInt(answer - 1).
// We specify the types of parameters and return values in function signatures, especially for callback functions in socket event handlers.
// optimization
// Reorganized the code to improve readability and maintainability.
// Moved the event handlers for the socket to the appropriate sections.
// Removed unnecessary comments and reordered functions.
// Simplified the socket.io-client import for the connect function.
// Removed the redundant console.log(args) and console.log(socket) statements.
// Added comments to describe the purpose of each section and function.
import * as readline from 'readline';
import { connect, Socket } from 'socket.io-client';
import { checkWin, checkDraw } from "./helper";

const args: string[] = process.argv.slice(2);

// Define the Readline interface
const rl: readline.Interface = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// Create a socket connection
const socket: Socket = connect(`http://${args[0]}:${args[1]}`);

// Event handler for socket connection
socket.on('connect', () => {
  console.log('Connected to the server.');
});

// Event handler for receiving a welcome message
socket.on('welcomeMsg', (msg: string) => {
  console.log(`Game started. You are the ${msg} player`);
});

// Event handler for receiving the game state
socket.on('gameState', (gameBoard: (string | null)[]) => {
  // Check if the game is over (win or draw)
  if (checkWin(gameBoard) || checkDraw(gameBoard)) {
    // Close the readline interface and disconnect from the server
    rl.close();
    socket.disconnect();
  } else {
    // Start making moves
    makeMove();
  }
});

// Event handler for receiving a move from the server
socket.on('move', ({ gameBoard }: {
  gameBoard: (string | null)[];
}) => {
  // Display the updated game board
  displayBoard(gameBoard);
});

// Event handler for receiving a game over message
socket.on('gameOver', (winner: string | null) => {
  // Display the game result
  if (winner) {
    console.log(`${winner}`);
  } else {
    console.log('Game is tied.');
  }
  // Disconnect from the server and close the readline interface
  socket.disconnect();
  rl.close();
});

// Event handler for handling extra players
socket.on('extraPlayer', () => {
  console.log('More than 2 players not allowed');
  // Disconnect from the server and close the readline interface
  socket.disconnect();
  rl.close();
});

// Display the game board
function displayBoard(gameBoard: (string | null)[]) {
  console.log('\nCurrent Board:');
  for (let i = 0; i < 9; i += 3) {
    console.log(
      ` ${gameBoard[i] || '. '} ${gameBoard[i + 1] || '. '} ${gameBoard[i + 2] || '. '}`
    );
  }
  console.log();
}

// Handle player moves
function makeMove() {
  rl.question('> ', (answer) => {
    if (answer === 'r') {
      // Handle resignation
      socket.emit('move', answer);
    } else {
      const index = parseInt(answer) - 1;
      if (Number.isNaN(index) || index < 0 || index > 8) {
        console.log('Invalid input. Please enter a number between 1 and 9.');
        // Retry move input
        makeMove();
      } else {
        // Send the valid move to the server
        socket.emit('move', index);
      }
    }
  });
}
