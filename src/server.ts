// conversion
// We use import statements instead of require.
// We declare the types of variables, function parameters, and function return types explicitly.
// We use type assertions (index as number) where necessary to indicate the correct type.
// We use const and let for variable declarations instead of var.
// We update the import for express, http, and socket.io to work with TypeScript.
// We use http.createServer(app) to create the HTTP server.
// We use Server from socket.io to create the socket.io server instance.
// We use server.listen to start the server.
// optimization
// Combined common code for "First" and "Second" players into a single function.
// Extracted the processMove function to handle game moves.
// Simplified the disconnectHandler function to handle disconnections.
// Removed unnecessary comments and separated concerns for better readability.
// Reordered function definitions for improved organization.
import express from "express";
import * as http from "http";
import { Server, Socket } from "socket.io"; // Import Socket type
import { checkWin, checkDraw } from "./helper";

const app = express();
const server = http.createServer(app);
const io = new Server(server);

const PORT = Number(process.argv[2]);
let connectCounter = 0;
let gameBoard: (string | null)[] = Array(9).fill(null);
let currentPlayer = "X";

app.use(express.static(__dirname + "/public"));

server.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});

// Emit welcome message and current game state to a socket
function emitGameState(socket: Socket, message: string) {
  socket.emit("welcomeMsg", message);
  socket.emit("gameState", gameBoard);
}

// Process a move received from a socket
function processMove(socket: Socket, index: string | number) {
  if (index === "r") {
    // Handle resignation
    io.emit(
      "gameOver",
      currentPlayer === "X"
        ? "Game won by second player due to resignation"
        : "Game won by first player due to resignation"
    );
  } else {
    if (gameBoard[index as number] === null) {
      // Make a valid move
      gameBoard[index as number] = currentPlayer === "X" ? "X" : "O";
      io.emit("move", { index, symbol: gameBoard[index as number], gameBoard });
      if (checkWin(gameBoard) || checkDraw(gameBoard)) {
        // Check if the game is won or a draw
        io.emit(
          "gameOver",
          currentPlayer === "X"
            ? "Game won by First player."
            : "Game won by Second player."
        );
        gameBoard = Array(9).fill(null);
      } else {
        // Switch to the next player's turn
        currentPlayer = currentPlayer === "X" ? "O" : "X";
      }
      socket.emit("gameState", gameBoard);
    }
  }
}

// Handle socket disconnection
function disconnectHandler() {
  currentPlayer = currentPlayer === "X" ? "X" : "O";
  connectCounter--;
  if (connectCounter === 1) {
    // If one player disconnects, declare the other player as the winner
    io.emit(
      "gameOver",
      currentPlayer === "X"
        ? "Game won by first player since second player disconnected"
        : "Game won by second player since first player disconnected"
    );
  }
}

io.on("connection", (socket: Socket) => {
  connectCounter++;

  if (connectCounter <= 2) {
    // Connect the first and second players
    emitGameState(socket, connectCounter === 1 ? "First" : "Second");

    socket.on("move", (index: string | number) => {
      // Handle game moves
      processMove(socket, index);
    });

    socket.on("disconnect", () => {
      // Handle disconnections
      disconnectHandler();
    });
  }

  if (connectCounter === 3) {
    // If a third player connects, emit an "extraPlayer" message
    socket.emit("extraPlayer");
    connectCounter--;
  }
});
